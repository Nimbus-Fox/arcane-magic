package com.nimbusfox.arcanemagic.proxies

import com.nimbusfox.arcanemagic.Mod.ModBlocks
import com.nimbusfox.arcanemagic.Mod.ModItems
import com.nimbusfox.arcanemagic.Mod.ModMaterials
import net.minecraft.init.Items
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraft.item.crafting.CraftingManager
import net.minecraft.item.crafting.IRecipe
import net.minecraft.util.ResourceLocation
import net.minecraftforge.event.RegistryEvent
import net.minecraftforge.event.entity.player.PlayerEvent.BreakSpeed
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.common.event.FMLInitializationEvent
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent
import net.minecraftforge.fml.common.registry.GameRegistry

@Mod.EventBusSubscriber
abstract class CommonProxy{
    open fun preInit(event: FMLPreInitializationEvent) {
        ModMaterials.createMaterials()
        ModItems.createItems()
        ModBlocks.createBlocks()
    }

    open fun init(event: FMLInitializationEvent) {

    }

    open fun postInit(event: FMLPostInitializationEvent) {

    }

    @SubscribeEvent
    fun registerItems(event: RegistryEvent.Register<Item>) {
        println("Loading items")
        event.registry.register(ModItems.speedGlyph)
    }

    @SubscribeEvent
    fun miningSpeed(event: BreakSpeed) {

    }

    @SubscribeEvent
    fun registerRecipes(event: RegistryEvent.Register<IRecipe>) {

    }
}