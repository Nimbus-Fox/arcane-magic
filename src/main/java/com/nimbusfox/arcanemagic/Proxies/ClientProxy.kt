package com.nimbusfox.arcanemagic.proxies

import com.google.common.eventbus.Subscribe
import com.nimbusfox.arcanemagic.ArcaneMagic
import com.nimbusfox.arcanemagic.Items.Bases.ItemGlyphBase
import com.nimbusfox.arcanemagic.Mod.ModItems
import com.nimbusfox.arcanemagic.proxies.CommonProxy
import net.minecraft.block.Block
import net.minecraft.client.renderer.block.model.ModelResourceLocation
import net.minecraft.item.Item
import net.minecraftforge.client.event.ModelRegistryEvent
import net.minecraftforge.client.model.ModelLoader
import net.minecraftforge.common.MinecraftForge
import net.minecraftforge.event.RegistryEvent
import net.minecraftforge.event.entity.player.ItemTooltipEvent
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent

@Mod.EventBusSubscriber
class ClientProxy : CommonProxy() {
    override fun preInit(event: FMLPreInitializationEvent) {
        super.preInit(event)

        MinecraftForge.EVENT_BUS.register(this)
    }

    @SubscribeEvent
    fun registerBlocks(event: RegistryEvent.Register<Block>) {

    }

    @SubscribeEvent
    fun registerModels(event: ModelRegistryEvent) {
        println("Loading models")
        ModelLoader.setCustomModelResourceLocation(ModItems.speedGlyph, 0, ModelResourceLocation(ModItems.speedGlyph.registryName, "inventory"))
    }

    @SubscribeEvent
    fun onToolTip(event: ItemTooltipEvent) {
        if (event.itemStack.hasTagCompound()) {
            if (event.itemStack.tagCompound!!.getBoolean("arcanemagic.hasGlyph")) {
                event.toolTip.add(1, " ")
                event.toolTip.add(2, ArcaneMagic.colorCode + "f" + ArcaneMagic.colorCode + "nGlyphs" + ArcaneMagic.colorCode + "r")

                if (event.itemStack.tagCompound!!.getBoolean("arcanemagic.speed1")) {
                    event.toolTip.add(3, ArcaneMagic.colorCode + "bSpeed I" + ArcaneMagic.colorCode + "r")
                }
            }
        }
    }
}