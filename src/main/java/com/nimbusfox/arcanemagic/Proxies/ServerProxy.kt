package com.nimbusfox.arcanemagic.proxies

import net.minecraftforge.common.MinecraftForge
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent

@Mod.EventBusSubscriber
class ServerProxy : CommonProxy() {
    override fun preInit(event: FMLPreInitializationEvent) {
        super.preInit(event)

        MinecraftForge.EVENT_BUS.register(this)
    }
}