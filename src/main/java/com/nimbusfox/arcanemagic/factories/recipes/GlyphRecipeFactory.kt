package com.nimbusfox.arcanemagic.factories.recipes

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonParseException
import com.google.gson.JsonSyntaxException
import com.nimbusfox.arcanemagic.ArcaneMagic
import com.nimbusfox.arcanemagic.Items.Bases.ItemGlyphBase
import net.minecraft.inventory.InventoryCrafting
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraft.item.crafting.IRecipe
import net.minecraft.item.crafting.Ingredient
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.util.JsonUtils
import net.minecraft.util.NonNullList
import net.minecraft.util.ResourceLocation
import net.minecraft.world.World
import net.minecraftforge.common.crafting.CraftingHelper
import net.minecraftforge.common.crafting.IRecipeFactory
import net.minecraftforge.common.crafting.JsonContext
import net.minecraftforge.oredict.ShapelessOreRecipe
import scala.util.parsing.json.`JSONObject$`

class GlyphRecipeFactory : IRecipeFactory {
    override fun parse(context: JsonContext?, json: JsonObject?): IRecipe {
        var type = JsonUtils.getString(json, "type")

        if (type == "add_glyph") {
            return AddGlyph(json!!, context!!)
        } else {
            throw JsonSyntaxException(ArcaneMagic.NAME + " received a recipe it does not recognise")
        }

        return null!!
    }

    companion object {
        fun getResult(json: JsonObject, context: JsonContext) : ItemStack {
            var resultObject = JsonUtils.getJsonObject(json, "result")

            var output = CraftingHelper.getItemStack(resultObject, context)

            var ings = NonNullList.create<Ingredient>()

            JsonUtils.getJsonArray(json, "ingredients").mapTo(ings) { CraftingHelper.getIngredient(it, context)}

            return output
        }

        private fun AddGlyph(json: JsonObject, context: JsonContext) : AddGlypghRecipe {
            var group = JsonUtils.getString(json, "group", "")

            var ings = NonNullList.create<Ingredient>()

            JsonUtils.getJsonArray(json, "ingredients").mapTo(ings) { CraftingHelper.getIngredient(it, context) }

            if (ings.isEmpty()) {
                throw JsonParseException("No ingredients for shapeless recipe")
            }

            return AddGlypghRecipe(if (group.isEmpty()) {
                null
            } else {
                ResourceLocation(group)
            }, ings, getResult(json, context))
        }

        class AddGlypghRecipe : ShapelessOreRecipe {
            constructor(group: ResourceLocation?, result: NonNullList<Ingredient>, primer: ItemStack) : super(group, result, primer)

            override fun getCraftingResult(crafting: InventoryCrafting): ItemStack {
                var result = super.getCraftingResult(crafting)

                if (!result.isEmpty) {

                    var i = 0

                    var tool = ItemStack.EMPTY
                    var glyph: ItemGlyphBase? = null

                    while (i < crafting.sizeInventory) {
                        var stack = crafting.getStackInSlot(i)

                        if (!stack.isEmpty) {
                            if (stack.item !is ItemGlyphBase) {
                                tool = stack
                            }

                            if (stack.item is ItemGlyphBase) {
                                glyph = stack.item as ItemGlyphBase
                            }
                        }

                        i += 1
                    }

                    result = tool.copy()

                    if (!result.hasTagCompound()) {
                        result.tagCompound = NBTTagCompound()
                    }

                    glyph!!.assignNBT(result)
                }

                return result
            }
        }
    }
}