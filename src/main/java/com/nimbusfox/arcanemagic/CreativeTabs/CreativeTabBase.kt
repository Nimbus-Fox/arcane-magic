package com.nimbusfox.arcanemagic.CreativeTabs

import net.minecraft.creativetab.CreativeTabs
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraftforge.fml.relauncher.Side
import net.minecraftforge.fml.relauncher.SideOnly

class CreativeTabBase : CreativeTabs {
    constructor(label: String, icon: String) : super(label) {
        itemStack = icon
    }

    companion object {
        private var itemStack : String? = null
    }

    @SideOnly(Side.CLIENT)
    override fun getTabIconItem() : ItemStack {
        return ItemStack(Item.getByNameOrId(itemStack))
    }
}