package com.nimbusfox.arcanemagic.Items.Bases

import net.minecraft.creativetab.CreativeTabs
import net.minecraft.item.Item
import net.minecraft.item.ItemStack

open class ItemGlyphBase : ItemBase {
    constructor(unlocalizedName: String, creativeTab: CreativeTabs, registryName: String) : super(unlocalizedName, creativeTab, registryName){

    }

    open fun assignNBT(item: ItemStack?) {
        item!!.tagCompound!!.setBoolean("arcanemagic.hasGlyph", true)
    }
}