package com.nimbusfox.arcanemagic.Items.Bases

import net.minecraft.creativetab.CreativeTabs
import net.minecraft.item.Item

open class ItemBase : Item {
    constructor(unlocalizedName: String, creativeTab: CreativeTabs, registryName: String) : super() {
        this.unlocalizedName = unlocalizedName
        this.creativeTab = creativeTab
        this.setRegistryName(registryName)
    }
}