package com.nimbusfox.arcanemagic.Items

import com.nimbusfox.arcanemagic.Items.Bases.ItemBase
import com.nimbusfox.arcanemagic.Items.Bases.ItemGlyphBase
import net.minecraft.creativetab.CreativeTabs
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraftforge.fml.common.registry.GameRegistry

class ItemSpeedGlyph : ItemGlyphBase {
    constructor(unlocalizedName: String, creativeTab: CreativeTabs, registryName: String) : super(unlocalizedName, creativeTab, registryName){

    }

    override fun assignNBT(item: ItemStack?) {
        if (item != null) {
            if (item.hasTagCompound()) {
                super.assignNBT(item)
                item.tagCompound!!.setBoolean("arcanemagic.speed1", true)
            }
        }
    }
}