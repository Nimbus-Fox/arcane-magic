package com.nimbusfox.arcanemagic.Materials

import net.minecraft.item.Item
import net.minecraftforge.common.util.EnumHelper


open class MaterialBase {
    var materialName: String = ""
    var harvestLevel: Int = 0
    var durability: Int = 0
    var miningSpeed: Float = 0.0F
    var damage: Float = 0.0F
    var enchantibility: Int = 0

    constructor(baseMaterial: Item.ToolMaterial) {
        harvestLevel = baseMaterial.harvestLevel
        durability = baseMaterial.maxUses
        miningSpeed = baseMaterial.efficiency
        damage = baseMaterial.attackDamage
        enchantibility = baseMaterial.enchantability
    }

    fun getMaterial() : Item.ToolMaterial {
        return EnumHelper.addToolMaterial(materialName, harvestLevel, durability, miningSpeed, damage, enchantibility)!!
    }
}