package com.nimbusfox.arcanemagic

import com.nimbusfox.arcanemagic.proxies.ClientProxy
import com.nimbusfox.arcanemagic.proxies.CommonProxy
import com.nimbusfox.arcanemagic.proxies.ServerProxy
import net.minecraft.client.Minecraft
import net.minecraftforge.common.MinecraftForge
import net.minecraftforge.fml.common.FMLCommonHandler
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.common.Mod.EventHandler
import net.minecraftforge.fml.common.SidedProxy
import net.minecraftforge.fml.common.event.FMLInitializationEvent
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent
import net.minecraftforge.fml.relauncher.Side

@Mod(modid = ArcaneMagic.MODID, version = ArcaneMagic.VERSION, name = ArcaneMagic.NAME)
class ArcaneMagic {

    companion object {
        const val MODID = "arcanemagic"
        const val VERSION = "0.1"
        const val NAME = "Arcane Magic"
        lateinit var proxy: CommonProxy
        const val colorCode = "§"
    }

    constructor() {
        if (Side.CLIENT.isClient) {
            proxy = ClientProxy()
        } else {
            proxy = ServerProxy()
        }
    }

    @Mod.EventHandler
    fun preInit(event: FMLPreInitializationEvent) {
        proxy.preInit(event)
    }

    @Mod.EventHandler
    fun init(event: FMLInitializationEvent) {
        proxy.init(event)
    }

    @Mod.EventHandler
    fun postInit(event: FMLPostInitializationEvent) {
        proxy.postInit(event)
    }
}
