package com.nimbusfox.arcanemagic.Blocks

import net.minecraft.block.Block
import net.minecraft.block.material.Material
import net.minecraft.creativetab.CreativeTabs

class BlockBase : Block {
    constructor(material: Material, unlocalizedName: String, creativeTab: CreativeTabs) : super(material) {
        this.unlocalizedName = unlocalizedName
        this.setCreativeTab(creativeTab)
    }
}