package com.nimbusfox.arcanemagic.Mod

import com.nimbusfox.arcanemagic.CreativeTabs.CreativeTabBase
import com.nimbusfox.arcanemagic.Items.ItemSpeedGlyph

class ModItems {

    companion object {
        fun createItems() {
            arcaneMagicTab = CreativeTabBase("arcanemagic", "Diamond")
            speedGlyph = ItemSpeedGlyph("arcanemagic.speedGlyph", arcaneMagicTab, "speedGlyph")
        }

        lateinit var arcaneMagicTab: CreativeTabBase
        lateinit var speedGlyph: ItemSpeedGlyph
    }

}